HEADERS += \
    $$PWD/TcpServer.h

SOURCES += \
    $$PWD/TcpServer.cpp

QT     += network

win32 {
    LIBS += -lws2_32
}
