//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "MlinkStreamReceiver.h"
#include <stdexcept>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <QTime>
#include <QTimer>
#include <QDebug>
#include <QCoreApplication>

#include "RawCheck.h"

using std::cerr;
using std::endl;
const int UDP_PORT_MSTREAM = 33301;
namespace {
const int RUN_LOOP_PERIOD_MS = 1000;
const int SEND_WORK_TIME_MS = 10;
const int WORK_SIZE_LIMIT = 100;
const int RECV_SELECT_TIMEOUT_US = 2e3; // 1 ms
const int RUN_TIME_CHECK_PERIOD_US = RUN_LOOP_PERIOD_MS*1e3/100; // Check run time 10 times
const int RUN_TIME_CHECK_PERIOD_CNT = RUN_TIME_CHECK_PERIOD_US/RECV_SELECT_TIMEOUT_US;
}

MlinkStreamReceiver::MlinkStreamReceiver(DeviceIndex index, const char *ipaddr, bool _bigFragId,
                                         quint16 hwBufSize, bool _doPrintStat)
    : QObject(nullptr),
      s_mstream(0),
      bigFragId(_bigFragId),
      runLoopTime(new QTime()),
      runLoopTimer(new QTimer(this)),
      rc(new RawCheck),
      sendWorkTimer(new QTimer(this)),
      taskNum(hwBufSize*100),
      taskIndexWrite(0),
      taskIndexSend(0),
      taskIndexErase(0),
      RECV_LIMIT(hwBufSize*4),
      msgsTt(RECV_LIMIT),
      iovecsTt(RECV_LIMIT),
      ackSizeLimit(1),
      stat(index, RECV_LIMIT),
      timeoutCnt(0),
      toDel(false),
      runTimeCheckPeriodCnt(RUN_TIME_CHECK_PERIOD_CNT),
      doPrintStat(_doPrintStat)
{
    freeSize.release(getFreeTask());
    buf.resize(taskNum*BUFLEN);
    tasks.resize(taskNum);
    for(uint i=0; i<taskNum; ++i) {
        tasks[i].buf = buf.data()+i*BUFLEN;
    }

    if (socket_init() == -1) {
        throw std::runtime_error("failed to init socket");
    }
    bind_mstream();

    addr_other = inet_addr(ipaddr);
    init_si_other();
    connect(runLoopTimer, &QTimer::timeout, this, &MlinkStreamReceiver::runLoop);
    runLoopTimer->setSingleShot(true);
    runLoopTimer->start(0);
    if(bigFragId)
        rc->setBigFragId();
#if (! SEM_INTERACT)
    connect(sendWorkTimer, &QTimer::timeout, this, &MlinkStreamReceiver::sendWork);
    sendWorkTimer->setSingleShot(true);
    sendWorkTimer->setInterval(SEND_WORK_TIME_MS);
#endif
}

MlinkStreamReceiver::~MlinkStreamReceiver()
{
    toDel = true;
    runLoopTimer->stop();

    QElapsedTimer timer;
    int loop=0;
    int curSemVal = testSem.available();
    while (getTaskToErase() && timer.elapsed()<1000) {
        ++loop;
        curSemVal = testSem.available();
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        curSemVal = testSem.available();
//        releaseData()
    }

    if(getTaskToErase()){
        qWarning() << "failed to terminate MlinkStreamReceiver";
    }

    delete runLoopTime;

    close(s_mstream);
}

void MlinkStreamReceiver::bind_mstream()
{
    s_mstream = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (-1 == s_mstream) {
#ifdef WIN32
        cerr << "socket error " << WSAGetLastError() << endl;
#else
        perror("socket error");
#endif
        throw std::runtime_error("socket error");
    }

    memset(static_cast<void *>(&si_me), 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
//    si_me.sin_port = htons(UDP_PORT_MSTREAM);
    si_me.sin_port = 0;
    if (-1 == bind(s_mstream, reinterpret_cast<const sockaddr *>(&si_me), sizeof(si_me))) {
#ifdef WIN32
        cerr << "bind error " << WSAGetLastError() << endl;
#else
        perror("bind error");
#endif
        throw std::runtime_error("bind error");
    } else {
        timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = RECV_SELECT_TIMEOUT_US/2;
        int ret = setsockopt(s_mstream, SOL_SOCKET, SO_RCVTIMEO, &tv,sizeof(tv));
        if(ret == -1)
            qWarning() << "setsockopt(s_mstream, SOL_SOCKET, SO_RCVTIMEO, &tv,sizeof(tv))=="<<ret;
        socklen_t len = sizeof(si_me);
        getsockname(s_mstream, reinterpret_cast<sockaddr *>(&si_me), &len);
    }
}


void MlinkStreamReceiver::init_si_other()
{
    memset(reinterpret_cast<char *>(&si_other), 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_addr.s_addr = addr_other;
    si_other.sin_port = htons(UDP_PORT_MSTREAM);
    cerr << QString("[MStream] [%1]: MlinkStreamReceiver receiving stream from %2:%3")
            .arg(stat.index.getSerialStr())
            .arg(inet_ntoa(si_other.sin_addr)).arg(ntohs(si_other.sin_port)).toStdString().c_str()
         << endl;
}

void MlinkStreamReceiver::printLoopStat(int loopCount, int loopTime_ms)
{
    int timePerLoopUs = loopTime_ms*1000/loopCount;
    if(doPrintStat) {
        QStringList sizeStat;
        for(int i=0; i<stat.pckSizeCnt.size(); ++i) {
            const quint32 &val = stat.pckSizeCnt[i];
            if(val)
                sizeStat << QString("pckSize%1=%2").arg(i).arg(val);
        }
        std::cerr << QString("[RecvStat]noSpaceLoop=%1;processDidntHelp=%2;"
                             "loopOk=%3;checkFailed=%4;noData=%5;"
                             "LoopCount=%6;LoopTimeMs=%7;Time_us/Loop=%8;%9\n")
                     .arg(stat.noFree).arg(stat.didntHelp)
                     .arg(stat.free).arg(stat.checkFail).arg(stat.noData)
                     .arg(loopCount).arg(loopTime_ms).arg(timePerLoopUs)
                     .arg(sizeStat.join(";"))
                     .toStdString().c_str();
    }

    emit stat_from_receiver_updated(stat);
    runTimeCheckPeriodCnt = timePerLoopUs ? qMax(1, RUN_TIME_CHECK_PERIOD_US/timePerLoopUs) : 1;
}

ssize_t MlinkStreamReceiver::recvfrom()
{
    mmsghdr *msgsPtr = &msgsTt[0];
    memset(msgsPtr, 0, sizeof(mmsghdr)*RECV_LIMIT);
    uint taskCandidat = taskIndexWrite;
    for (int i = 0; i < RECV_LIMIT; i++) {
        iovecsTt[i].iov_base         = tasks[taskCandidat].buf;
        iovecsTt[i].iov_len          = BUFLEN;
        msgsTt[i].msg_hdr.msg_iov    = &iovecsTt[i];
        msgsTt[i].msg_hdr.msg_iovlen = 1;
        shiftIndex(taskCandidat);
    }

    int retval = 0;
    try {
        retval = recvmmsg(s_mstream, msgsPtr, RECV_LIMIT, MSG_WAITFORONE, nullptr);
    } catch (const std::exception &e) {
        cerr << "Error while recvmmsg:" << e.what() << endl;
    } catch (...) {
        cerr << "Some err while recvmmsg" << endl;
    }
    if(retval > 0) {
        ++stat.pckSizeCnt[retval];
        taskCandidat = taskIndexWrite;
        for(int i=0; i<retval; ++i) {
            tasks[taskCandidat].len = msgsTt[i].msg_len;
            shiftIndex(taskCandidat);
        }
    } else {
        if(retval==0) {
                ++stat.pckSizeCnt[retval];
        } else {
            if(errno == EAGAIN) {
                ++stat.pckSizeCnt[0];
                retval = 0;
            } else {
                perror("MlinkStreamReceiver::recvfrom() recvmmsg(...) err:");
            }
        }
    }
    return retval;
}

void MlinkStreamReceiver::send_ack(const MLinkFrameInfo &fi)
{
    send_ack(std::vector<MLinkFrameInfo>(1, fi));
}

void MlinkStreamReceiver::send_ack(const std::vector<MLinkFrameInfo> &fiVec)
{
    const size_t fiNum = fiVec.size();
    if(fiNum==0)
        return;

    const MLinkFrameInfo &fi = fiVec[0];
//    qInfo() << "Send ack seq:" << fi.hdr.seq;
    size_t i;
    const size_t data_words = 1+fiNum;
    const size_t bufsize = sizeof(MLinkFrameHdr) + 4*data_words + 4;
    char buf[bufsize];
    MLinkFrameHdr *hdr = reinterpret_cast<MLinkFrameHdr *>(buf);
    uint32_t *h = reinterpret_cast<uint32_t *>(&buf[sizeof(MLinkFrameHdr)]);
    uint32_t *crc = reinterpret_cast<uint32_t *>(&buf[sizeof(MLinkFrameHdr)+4*data_words]);
    //    struct sockaddr_in si_other;
    //    memset((char *) &si_other, 0, sizeof(si_other));
    //    si_other.sin_family = AF_INET;
    //    si_other.sin_port = htons(UDP_PORT);
    //    si_other.sin_addr.s_addr = htonl(dest_addr);
    hdr->sync = ML_FRAME_SYNC;
    hdr->type = FRAME_TYPE_MSTREAM;
    hdr->seq = fi.hdr.seq;
    hdr->len = static_cast<uint16_t>(4+data_words);
    hdr->src = fi.hdr.dst;
    hdr->dst = fi.hdr.src;
//    memset(h, 0, data_words*4);
    const uint8_t dtype = 1;
    const uint16_t flen = static_cast<uint16_t>(4*(data_words-2));
    h[0] = (dtype<<24) | (1<<22) | flen; // ACK 22 bit, flen
    for(i=0; i<fiNum; ++i) {
        const MLinkFrameInfo &fragInfo = fiVec[i];
        if(bigFragId) {
            h[1+i] = static_cast<uint32_t>(((fragInfo.fid&0xFFFF) << 16) | (fragInfo.foff &0xFFFF));
        } else {
            h[1+i] = static_cast<uint32_t>(((fragInfo.fid&0xFF) << 24) | (fragInfo.foff &0xFFFFFF));
        }
    }
    *crc = 0;

    int ret = sendto(s_mstream, buf, bufsize, 0,
                     reinterpret_cast<const sockaddr *>(&si_other), sizeof(si_other));
    if (ret == -1) {
#ifdef WIN32
        cerr << "socket error " << WSAGetLastError() << endl;
#else
        perror("sendto");
#endif
    }
}

void MlinkStreamReceiver::connectToHardware()
{
    MLinkFrameInfo fi;
    fi.fid = -1;
    fi.foff = -1;
    fi.hdr.seq = 0;
    fi.hdr.src = 1;
    fi.hdr.dst = 0xFEFE;
    send_ack(fi);
}

void MlinkStreamReceiver::runLoop()
{
    runLoopTime->restart();
    stat.clear(static_cast<int>(RECV_LIMIT));
    int loopCount = 0; // reduce number of timer checks, see SELECT_TIMEOUT_US

    while(true) {
        if((++loopCount) % runTimeCheckPeriodCnt == 0){
            int ms = runLoopTime->elapsed();
             if(ms > RUN_LOOP_PERIOD_MS) {
                 printLoopStat(loopCount, runLoopTime->restart());
                 loopCount=0;
             }
             QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers, 1);
        }

        if(toDel) return;
        // Let timer to do its work
#if SEM_INTERACT
        if(freeSize.available() < RECV_LIMIT) {
            ++stat.noFree;
            if(freeSize.tryAcquire(RECV_LIMIT, 100)) {
                freeSize.release(RECV_LIMIT);
            } else {
                QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers, 0);
                ++stat.didntHelp;
                continue;
            }
        } else
            ++stat.free;
#else
        if(!hasFreeTask(RECV_LIMIT)) {
            ++stat.noFree;
            if(getTodoTask())
                sendWork();
            if(getTaskToErase()) {
                QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers, 0);
                if(toDel) return;
            }
            if(!hasFreeTask(RECV_LIMIT)) {
                ++stat.didntHelp;
                continue;
            }
        } else {
            ++stat.free;
        }
#endif
        const int pckNum = recvfrom();
        if(pckNum > 0) {
            for(int i=0; i<pckNum; ++i) {
                Task &task = tasks[taskIndexWrite];
                if(task.len<=0) {
                    task.dataValid = false;
                    shiftIndex(taskIndexWrite);
                    continue;
                }
                // Send to decode thread
                if (rc->check_header(task.buf, task.len, &task.fi)) {
#if (! SEM_INTERACT)
                    if(getTodoTask()==0)
                        sendWorkTimer->start();
#endif
                    task.dataValid = true;
                    ackStack.push_back(task.fi);
//                    if(ackStack.size()==1) {
                    if(ackStack.size()==ackSizeLimit) {
                        send_ack(ackStack);
                        ackStack.clear();
                    }
                } else {
                    task.dataValid = false;
                    ++stat.checkFail;
                }
                shiftIndex(taskIndexWrite);
            }
            send_ack(ackStack);
            ackStack.clear();
#if SEM_INTERACT
            dataSize.release(pckNum);
            freeSize.acquire(pckNum);
#endif
        } else {
            if(pckNum == 0) {
                ++stat.noData;
                continue;
            } else {
                qDebug() << "========= recvSize < 0 ==========";
                continue;
            }
        }

#if (! SEM_INTERACT)
        if(getTodoTask() > WORK_SIZE_LIMIT)
            sendWork();
#endif
    }
}

#if (! SEM_INTERACT)
void MlinkStreamReceiver::releaseData(int shift)
{
    testSem.acquire(shift);
    Q_ASSERT(getTaskToErase() >= shift);
    shiftIndex(taskIndexErase, shift);
}

void MlinkStreamReceiver::sendWork()
{
    const int todoTask = getTodoTask();
    if(toDel || todoTask==0)
        return;
    sendtasks.clear();
    for(int i=0; i<todoTask; ++i) {
        const auto &task = tasks[taskIndexSend];
        sendtasks.push_back(task);
        shiftIndex(taskIndexSend);
    }
    emit gotData(sendtasks);
//    releaseData(sendtasks);
}
#endif

void MlinkStreamReceiver::deleteReceiver()
{
    toDel = true;
    deleteLater();
}
