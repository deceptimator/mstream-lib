//
//    Copyright 2011-2016 Ivan Filippov
//    Copyright 2011-2016 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "MStreamDump.h"

#include "MStreamFileWriter.h"
#include "MlinkStreamReceiver.h"
#include "pnp-server/pnp-server/PNPServer.h"
#include "mldiscover/MldpListener.h"
#include "daq-config/DaqConfig.h"
#include "metric-sender/MetricSender.h"
#include "base/threadQuit.h"
#include "mldiscover/LldpDiscovery.h"
#include "RawCheck.h"
#include "tcpserver/TcpServer.h"

#include <QCoreApplication>
#include <QDebug>
#include <QNetworkInterface>
#include <QStringList>
#include <QThread>
#include <QTime>
#include <QTimer>

#include <cstring> // memcpy
#include <iostream> // cerr
#ifdef Q_OS_UNIX
#include <unistd.h>
#endif

#define PRINT_FRAMES false

namespace {
const quint32 MLINK_DATA_PADDING_MAGIC = 0x12206249;
const int BUF_HISTORY_SIZE = 800;
}

MStreamDump::MStreamDump(MStreamDumpParameters msParams, bool _dumpAsApp) :
    QObject(),
    MStreamDumpParameters(msParams),
    mr(nullptr),
    mrDataSizeSem(nullptr),
    receiverThread(new QThread(this)),
    fw(nullptr),
    tcpServerThread(new QThread(this)),
    tcpServer(new TcpServer(tcpPort, nullptr, QString("%1").arg(getSerialIdStr(true)))),
    statTime(new QTime()),
    statTimer(new QTimer(this)),
    waitDataTimer(new QTimer(this)),
    metricTimer(new QTimer(this)),
    isDevBusy(false),
    myReceiverPort(0),
    toDel(false),
    dumpAsApp(_dumpAsApp)
{
    qRegisterMetaType<MLinkFrameInfo>();
    qRegisterMetaType<TcpServerStatus>();
    qRegisterMetaType<QVector<Task> >();
    qRegisterMetaType<ReceiverStat>();
    stat.index = msParams.deviceIndex;

    msdump_reset(deviceIndex, bigFragId, hwBufSize, ackSizeLimit, fileName, true);

    tcpPort = tcpServer->getServerPort();
    tcpServerThread->setObjectName(QString("TcpS_%1").arg(getSerialIdStr(true)));
    tcpServer->moveToThread(tcpServerThread);
    tcpServerThread->start();
    connect(this, &MStreamDump::closing, tcpServer, &TcpServer::deleteLater);
    connect(tcpServer, &TcpServer::destroyed, tcpServerThread, &QThread::quit);
    connect(tcpServer, &TcpServer::statusUpdated, this, &MStreamDump::tcpServerStatusUpdated);
    connect(this, &MStreamDump::reset, tcpServer, &TcpServer::reset);

    receiverThread->setObjectName(QString("MlR_%1").arg(getSerialIdStr(true)));

    updateProgDescription();

    connect(statTimer, &QTimer::timeout, this, &MStreamDump::printStat);
    statTimer->setSingleShot(false);
    statTimer->start(1000);

#if SEM_INTERACT
    connect(waitDataTimer, &QTimer::timeout, this, &MStreamDump::waitForData);
    waitDataTimer->setInterval(0);
    waitDataTimer->setSingleShot(true);
#endif

    metricTimer->start(5000);
    connect(metricTimer, &QTimer::timeout, this, &MStreamDump::sendMetrics);
    dataIndex = 0;

    if(!deviceAddress.isNull())
        createReciever();
}

MStreamDump::~MStreamDump()
{
    toDel = true;
    statTimer->stop();
    emit closing(progDescr);
    QTime timer;
    timer.start();

    while(tcpServerThread->isRunning() && timer.elapsed()<1000){
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 50);
    }

    if(tcpServerThread->isRunning()){
        qWarning() << logPrefix() + "tcpServerThread isn't stopped";
        tcpServerThread->terminate();
    }

    if(fw) delete fw;

    destroyReciever();

    delete statTime;
}

void MStreamDump::start()
{
    statTimer->start(1000);
}

void MStreamDump::terminate()
{
    statTimer->stop();
    emit closing(progDescr);
    emit deleteLater();
}

void MStreamDump::gotData(const QVector<Task> &tasks)
{
    if(!mr)
        return;
#if SEM_INTERACT
    Q_UNUSED(tasks)
#else
    for (const Task &task : tasks) {
        if(task.len && task.buf[0]==0)
            processData(task.buf, task.len, task.fi);
        if(task.dataValid)
            processData(task.buf, task.len, task.fi);
    }
    int size = tasks.size();
    mr->testSem.release(size);
    emit releaseData(size);
#endif
}

void MStreamDump::processData(const char *buf, int recvSize, const MLinkFrameInfo &fi)
{
    Q_ASSERT(recvSize>0);

    stat.recvDataSize += recvSize;
    ++stat.packageCount;

    if(PRINT_FRAMES) {
        qInfo().noquote() <<endl<<endl<<endl<< logPrefix() +QString("seq ")<<fi.hdr.seq<<" recvSize="<<recvSize;
    }
    // no need to check crc
//        quint32 crc = *(quint32*)(&buf[recv_size-4]);
//        addHistory(fi.fid); // wrong: add just 1st fid in mlink-package
//        continue;

    if (recvSize < (int)(sizeof(MLinkFrameHdr) + sizeof(quint32))) {
        qWarning() << logPrefix() + QString("Received datagram too small: recvSize=%1").arg(recvSize);
        ++stat.badFormatPackage;
        return;
    }

    int curPos = sizeof(MLinkFrameHdr);
    const quint32 *crc = reinterpret_cast<const quint32 *>(&buf[recvSize-sizeof(quint32)]);
    Q_ASSERT(*crc == MLINK_DATA_PADDING_MAGIC);
    if (*crc != MLINK_DATA_PADDING_MAGIC) {
        qWarning() << logPrefix() + QString("Bad magic after MLinkFrame: %1, expected %21")
                      .arg(*crc, 8, 16, QChar('0'))
                      .arg(MLINK_DATA_PADDING_MAGIC, 8, 16, QChar('0'));
        ++stat.badFormatPackage;
        return;
    }

    bool mstreamError = false;
    while(recvSize - (int)sizeof(quint32) > curPos) { // -crcSize
        if (recvSize-curPos < (int)sizeof(MStreamHeader)){
            qWarning() << logPrefix() + QString("Can't read MStreamHeader: truncated frame, curPos=%1, recvSize=%2, seqNum=%3")
                          .arg(curPos).arg(recvSize).arg(fi.hdr.seq);
            mstreamError = true;
            break;
        }
        fragmentHdr = reinterpret_cast<const MStreamFragmentHeader *>(&buf[curPos]);
        curPos += sizeof(MStreamFragmentHeader);
        if (recvSize-curPos < (int)(fragmentHdr->fragmentLength+sizeof(uint32_t))){
            qWarning() << logPrefix() + QString("Can't read MStream fragment with crc: truncated frame, curPos=%1").arg(curPos);
            qWarning() << logPrefix() + "fragmentHdr->fragmentLength=" << fragmentHdr->fragmentLength << "; crc="<<4;
            qWarning() << logPrefix() + "rest size:" << recvSize-curPos;
            mstreamError = true;
            break;
        }
        if(PRINT_FRAMES) {
            qInfo()<< logPrefix() + "fid="<<fragmentHdr->getFragId(bigFragId)<<" len="<<fragmentHdr->fragmentLength;
        }

        if (fragmentHdr->fragmentLength == 0){
            qWarning() << logPrefix() + QString("Invalid MStream header: fragmentLength=0, curPos=%1").arg(curPos);
            Q_ASSERT(fragmentHdr->fragmentLength); // to detect empty fragment in debug
            curPos += fragmentHdr->fragmentLength+sizeof(uint32_t); // payload+crc
            mstreamError = true;
            continue;
        }
        const int dataPos = curPos;
        curPos += fragmentHdr->fragmentLength; // payload

        addHistory(fragmentHdr->getFragId(bigFragId));

        quint16 idDiff = lastClosedFragmentId-fragmentHdr->getFragId(bigFragId);
        if(!bigFragId) idDiff &= 0xFF;
        if(!initStage){
        if (idDiff < getAcceptedRange() || idDiff == 0) {
            ++stat.retransmitedFragParts;
            // Smart check for fragmentId resetted
//                bool ret = isFragIdResetted();
//                quint16 curFrId = fragmentHdr->getFragId(bigFragId);
            // qDebug()<<"Got retransmited fragment: lastClosedFragmentId="<<lastClosedFragmentId<<" curFrId="<<curFrId;
            break;
//                //Frame re-transmited
//                QSet<quint16> previousFrId;
//                previousFrId.insert(curFrId);
//                for(int shift=1; shift<BUF_HISTORY_SIZE; ++shift){
//                    int pos = histPos-shift;
//                    if(pos<0)
//                        pos += BUF_HISTORY_SIZE;
//                    previousFrId.insert(histFrags[pos]);
//                    if(previousFrId.size() == getAcceptedRange()+1)
//                        break;
//                }
//                QString prevIdStr("previous fragId:");
//                foreach (quint16 id, previousFrId) {
//                    prevIdStr.append(QString("%1 ").arg(id));
//                }
//                qDebug()<<prevIdStr;

//                bool ok=true;
//                if(previousFrId.size() != getAcceptedRange()+1){
//                    qWarning() << logPrefix() + "Insufficient fragId history";
//                    ok=false;
//                } else {
//                    qDebug()<<"lastClosedFragmentId="<<lastClosedFragmentId<<" curFrId="<<curFrId;

//                    foreach (quint16 id, previousFrId) {
//                        quint16 diff = curFrId-id;
//                        if(!bigFragId) diff &= 0xFF;
//                        if(diff > 2*getAcceptedRange()-1){
//                            qDebug()<<"diff(curFrId-if)="<<diff<<" curFrId="<<curFrId<<" id="<<id;
//                            ok=false;
//                            break;
//                        }
//                    }
//                }

//                if(ok){
//                    qDebug()<<"Check passed; Start closing all pending fragments";
//                    quint16 bakFrId = lastClosedFragmentId;
//                    quint16 frId=lastClosedFragmentId+1;
//                    if(!bigFragId) frId &= 0xFF;
//                    for(; frId!=bakFrId; ++frId) {
//                        if(frId==fragmentHdr->getFragId(bigFragId))
//                            continue;
//                        BaseFragment &fragment = fragments[frId];
//                        if(fragment.free) {
//                            lastClosedFragmentId = frId;
//                            continue;
//                        }
//                        qDebug()<<"Force closing: fragmentId="<<frId;
//                        closeFragment(frId, true);
//                    }

//                    lastClosedFragmentId = fragmentHdr->getFragId(bigFragId)-1;
//                    if(!bigFragId) lastClosedFragmentId &= 0xFF;
//                    qDebug() << "Set lastClosedFragmentId=" << lastClosedFragmentId
//                             << " previous lastClosedFragmentId=" << bakFrId;
//                } else {
//                    qDebug()<<"Check failed";
//                    ++stat.retransmitedFragParts;
//                    continue;
//                }
        } else {
            idDiff = fragmentHdr->getFragId(bigFragId)-lastClosedFragmentId;
            if(!bigFragId) idDiff &= 0xFF;

            if(idDiff>getAcceptedRange()) {
                // Check on fragment loss
                qWarning() << logPrefix() + "Force closing previous; reinit."<<"lastClosedFragmentId:"<<lastClosedFragmentId
                        <<"curFrId:"<<fragmentHdr->getFragId(bigFragId) << "idDiff:"<<idDiff;
                if(!adcTrigInfo.free) {
                    closeAdc();
                }
                initStage=true;
            }
        }
        } // if(!initStage)


//            addHistory(fragmentHdr->fragmentId);
        BaseFragment &fragment = fragments[fragmentHdr->getFragId(bigFragId)];
        if (fragment.free){
            //                qDebug() << "init new fragment id="<<fragment.fragmentId;
            ++stat.fragTotal;
            fragment.free = false;
            fragment.deviceId = fragmentHdr->deviceId;
            fragment.subType = fragmentHdr->subType;
            fragment.data.resize(sizeof(MStreamFragmentHeader));
            MStreamFragmentHeader *h = (MStreamFragmentHeader *)fragment.data.data();
            std::memcpy(h, fragmentHdr, sizeof(MStreamFragmentHeader));
        }

        const int start = fragmentHdr->getFragOffset(bigFragId);
        const int end = start + fragmentHdr->fragmentLength;

        if (fragment.parts.contains(start) && fragment.parts[start]==end){
            //Got re-transmited fragment's part of incomplited package
            ++stat.retransmitedFragParts;
            /*
            qDebug() << "got retransmited fragment part:lastClosedFragmentId=" << lastClosedFragmentId
                     << "cur fragId=" << fragmentHdr->fragmentId
                     << "start=" << start
                     << "end=" << end ;
                     */
            break;
        }

        if (initStage)
            initPackageSet.insert(fi.fid);

        if(fragment.completed){
//            qWarning() << logPrefix() + QString("Recived fragment is already completed; fId=%1").arg(fragment.fragmentId);
#ifdef QT_DEBUG
//            printHistory();
#endif
            continue;
        }
        Q_ASSERT(!fragment.completed);
        //            Q_ASSERT(!fragment.parts.contains(mStreamHdr->fragmentOffset));

        if (fragment.data.size() < end+(int)sizeof(MStreamFragmentHeader))
            fragment.data.resize(end+sizeof(MStreamFragmentHeader));
        fragment.parts[start] = end;
        std::memcpy(fragment.data.data()+start+sizeof(MStreamFragmentHeader), &buf[dataPos], fragmentHdr->fragmentLength);

        const bool lastFragment = (fragmentHdr->flgs >> 5) & 0x1;          //last fragment
        if (lastFragment){
            fragment.expectedLen = end;
        }
//            qDebug() << "add new part of fragment"<<fragmentHdr->fragmentId << " from:"<<start<<"to:"<<end<<(lastFragment?" last fragment":"");
        if (initStage) {
            if (initPackageSet.size() > hwBufSize)
                initialize();
        }
        if (fragment.expectedLen){
            while(fragment.parts.contains(fragment.curLen)) {
                fragment.curLen = fragment.parts[fragment.curLen];
            }
            if (fragment.curLen == fragment.expectedLen){
                fragment.completed = true;
//                    qDebug() << "fragment"<<fragmentHdr->fragmentId<<"completed; evNumber="<<fragmentHdr->evNum;
//                    qDebug() << "fragment"<<fragmentHdr->fragmentId<<"completed;";
                if (!initStage) {
                    if (fragmentHdr->getFragId(bigFragId) == ((lastClosedFragmentId+1)%(bigFragId?65536:256)))
                        closeFragment(fragmentHdr->getFragId(bigFragId));
                    else {
                        quint16 diff = fragmentHdr->getFragId(bigFragId)-lastClosedFragmentId;
                        if(!bigFragId) diff &= 0xFF;
                        if (diff > getAcceptedRange()){
                            // Got completed fragment after initialization stage which frId is too big
                            qWarning() << logPrefix() <<"some fragments stocked;"
                                       <<"lastClosedFragmentId="<<lastClosedFragmentId
                                      <<"bufSize="<<hwBufSize
                                     <<"curFrId="<<fragmentHdr->getFragId(bigFragId); //TODO: sad
                            printHistory();
                            closeFragment((lastClosedFragmentId+1)%(bigFragId?65536:256), true);
                            qWarning() << logPrefix() + QString("framesTotal=%1").arg(stat.fragTotal);
                            qWarning() << logPrefix() + QString("framesSkipped=%1").arg(stat.fragDroped);
                        }
                    }
                    // else got fragment for uncompleted event (adc64 case)
                }
            }
        }
    }

    if (mstreamError)
        ++stat.badFormatPackage;
    else
        ++stat.goodFormatPackage;

    if(debugPrint){
        debugPrintBufInHex(buf, recvSize);
        debugPrint=false;
    }
}

void MStreamDump::lldpCacheUpdated(const LldpInfoCache &dd)
{
    const auto it = dd.find(deviceIndex);
    if (it == dd.end())
        return;

    const auto &lldp = it.value();
    if(lldp.mstream_locked) {
        bool isLockedByMe = false;
        QHostAddress mstreamHost(lldp.mstream_ip);
        foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
            if (address.protocol() == QAbstractSocket::IPv4Protocol
                    && address != QHostAddress(QHostAddress::LocalHost)) {
                if(address == mstreamHost) {
                    isLockedByMe = true;
                    break;
                }
            }
        }
        if(!isLockedByMe) {
            if(!isDevBusy) {
                qWarning() << logPrefix() +  "Device is lock to another host";
                isDevBusy = true;
            }
            destroyReciever();
            return;
        }
    }
    isDevBusy = false;

    QHostAddress newHost(lldp.ip_addr);
    if(deviceAddress != newHost) {
        destroyReciever();
        deviceAddress = newHost;
        createReciever();
        updateProgDescription();
        //            runLoop();
    } else {
        if(!lldp.mstream_locked)
            emit connectToHardware();
    }
}

void MStreamDump::tcpServerStatusUpdated(const TcpServerStatus &status)
{
    tcpStatus = status;
    updateProgDescription();
}


//On force closing we should send this fragment (if we can, if trig frame completed)
//and others assoshiated with this event
void MStreamDump::closeFragment(quint16 fragmentId, bool force)
{
    BaseFragment *fragment = &fragments[fragmentId];

//    printHistory();
    Q_ASSERT((lastClosedFragmentId+1)%(bigFragId?65536:256) == fragmentId);
    if (fragment->free || !fragment->completed) {
        Q_ASSERT(force);
        if(fragment->free) {
            // Device skiped all fragments with id=fragmentId
            qWarning() << logPrefix() + QString("Closing empty fragment #%1").arg(fragmentId);
        } else {
            // Device skiped some/any fragment(s) with id=fragmentId
            qWarning() << logPrefix() + QString("Closing uncompleted fragment #%1").arg(fragmentId);
            fragment->clear();
            ++stat.fragIncompled;
        }

        ++lastClosedFragmentId; // same as lastClosedFragmentId = fragmentId;
        ++stat.fragDroped;
        ++fragmentId;
        if(!bigFragId) fragmentId &= 0xFF;
        fragment = &fragments[fragmentId];
        if(fragment->free || !fragment->completed)
            return;
        force = false;
        MStreamHeader *mStreamHdr = reinterpret_cast<MStreamHeader *>(fragment->data.data());
        const quint32 evNum = mStreamHdr->evNum;
        fragment->evNum = evNum;
        qDebug()<< logPrefix() + "After force closing; Good fragment fragId="<<fragmentId<<"; evNum="<<fragment->evNum;
    }

    do {
        // Normal completed fragment
        MStreamHeader *mStreamHdr = reinterpret_cast<MStreamHeader *>(fragment->data.data());
        const quint32 evNum = mStreamHdr->evNum;
        fragment->evNum = evNum;
        fragment->deviceSerial = mStreamHdr->deviceSerial;

        checkDeviceSerial(mStreamHdr->deviceSerial);

        if (fragment->closed && !force) {
            qWarning() << logPrefix() << "double close fragment frId="<<fragmentId
                     <<" ev="<<fragment->evNum<<" subType="<<fragment->subType;
            printHistory();
            Q_ASSERT(!fragment->closed);
        }
        ++lastClosedFragmentId; // same as lastClosedFragmentId = fragmentId;
        if(!bigFragId) lastClosedFragmentId &= 0xFF;
        Q_ASSERT(lastClosedFragmentId == fragmentId);
        ++stat.fragClosed;
        fragment->closed=true;
        //        qDebug() << "Fragment"<<fragmentId<<"closed evNum"<<fragment->evNum<<" subType="<<fragment->subType;
        switch (fragment->deviceId) {
        case 0xca: // adc64wr
        case 0xc5: // adc64s2
        case 0xd4: // adc64ve
        case 0xd9: // adc64ve-xge
            if(!adcTrigInfo.free && adcTrigInfo.evNum != evNum){
                qWarning() << logPrefix() << "force closing previous adc event evNum="<<adcTrigInfo.evNum << "curEvNum="<<evNum << " curFrId=" <<fragmentId;
//                printHistory();
//                qDebug()<<"last Event fragments: "<<lastEventFragments.first<<" - "<<lastEventFragments.second;
                closeAdc();
//                qDebug()<<"last Event fragments: "<<lastEventFragments.first<<" - "<<lastEventFragments.second;
                qWarning()<< logPrefix() + QString("curFrId=%1 type=%2 evNum=%3 ch=%4")
                          .arg(fragment->fragmentId)
                          .arg(fragment->subType)
                          .arg(fragment->evNum)
                          .arg(mStreamHdr->userDefBits,2,16,QChar('0'));
                qDebug()<< logPrefix() + "Start Bad fragment";
                debugPrintBufInHex(fragment->data.data(), fragment->data.size());
                qDebug()<< logPrefix() + "End Bad fragment";
                debugPrint=true;
            }
            if(adcTrigInfo.free){
                adcTrigInfo.free = false;
                adcTrigInfo.evNum = evNum;
            }
            adcTrigInfo.totalDataSize += fragment->curLen - (sizeof(MStreamHeader)-sizeof(MStreamFragmentHeader));
            if (fragment->subType==MS_SUB_TYPE_TRIG){
                if(fragment->curLen != (sizeof(MStreamHeader)-sizeof(MStreamFragmentHeader))+sizeof(AdcMStreamTrigPayload))
                    qWarning() << logPrefix() + "fragment->curLen:" << fragment->curLen;
                Q_ASSERT(fragment->curLen == (sizeof(MStreamHeader)-sizeof(MStreamFragmentHeader))+sizeof(AdcMStreamTrigPayload));

                if (adcTrigInfo.gotInfo){
                    qWarning() << logPrefix() + QString("Got duplicate AdcTrigInfo for evNum=%1").arg(evNum);
                }
                adcTrigInfo.gotInfo = true;
                adcTrigInfo.trigFrag = fragment;

                AdcMStreamTrigPayload *adcTrig = (AdcMStreamTrigPayload*)(fragment->data.data()+sizeof(MStreamHeader));
                adcTrigInfo.workedChannels = ((uint64_t)adcTrig->hiCh << 32) | adcTrig->lowCh;

                if (adcTrigInfo.workedChannels == adcTrigInfo.currentChannels){
//                    qDebug() << "Event "<<evNum<<" completed";
                    closeAdc();
                }
            } else {
                Q_ASSERT(fragment->subType==MS_SUB_TYPE_DATA);

                quint8 &chNum = mStreamHdr->userDefBits;
//                qDebug()<<fragmentId<<": data ch="<<chNum<<" evNum="<<evNum;
                adcTrigInfo.currentChannels |= (quint64)1<<chNum;
                adcTrigInfo.fragments[chNum] = fragment;
                if (adcTrigInfo.gotInfo && adcTrigInfo.workedChannels == adcTrigInfo.currentChannels){
//                    qDebug() << "Event "<<evNum<<" completed";
                    closeAdc();
                }
            }
            break;
        case 0xc2: // HRB6ASD
        case 0xcf: // TTVXS
        case 0xd0: // TDC72VXS
        case 0xd3: // TDC64VHLE
        case 0xd6: // TQDC16VS
        {
            Q_ASSERT(fragment->subType==MS_SUB_TYPE_TRIG);
            closeHrb(*fragment, mStreamHdr->userDefBits, fragment->deviceId != 0xc2);
//            qDebug() << "Fragment id="<<fragment->fragmentId<<" cleared.";
            break;
        }
        default:
            qWarning() << logPrefix() + QString("Unknown device type %1").arg(fragment->deviceId, 2, 16, QChar('0'));
            closeHrb(*fragment, mStreamHdr->userDefBits);
            Q_ASSERT(false);
            break;
        }
        //check is there a completed fargment
        ++fragmentId;
        if(!bigFragId) fragmentId &= 0xFF;
        fragment = &fragments[fragmentId];
    } while(fragment->completed);
}


void MStreamDump::closeAdc()
{
    QMap<quint8, BaseFragment*>::const_iterator it;
    if(!adcTrigInfo.gotInfo) {
        // Can't send without trig fragment
        qWarning() << logPrefix() + "closeAdc without adcTrigInfo";
        for (it=adcTrigInfo.fragments.constBegin(); it!=adcTrigInfo.fragments.constEnd(); ++it){
            qInfo() << logPrefix() +  "drop frId:"<<it.value()->fragmentId;
            it.value()->clear();
            ++stat.fragDroped;
        }
        adcTrigInfo.clear();
        return;
    }
    // fragment.curLen - length in bytes =8+Subtype 0 Payload
    int fragmentsNumber = adcTrigInfo.fragments.size();
    if(adcTrigInfo.gotInfo) {
        if(mpdDeviceHdr.deviceSerial == adcTrigInfo.trigFrag->deviceSerial){
            // Skip first adc-event
            const quint32 expectedEvNum = (mpdEventHdr.evNum+1)&0xFFFFFFu;
            if(expectedEvNum != adcTrigInfo.trigFrag->evNum){
                const quint32 diff = (adcTrigInfo.trigFrag->evNum-expectedEvNum)&0xFFFFFFu;
                ++stat.missedEventsRanges;
                stat.missedEvents += diff;
            }
        }
        mpdDeviceHdr.deviceId = adcTrigInfo.trigFrag->deviceId;
        mpdDeviceHdr.deviceSerial = adcTrigInfo.trigFrag->deviceSerial;
        mpdEventHdr.evNum = adcTrigInfo.trigFrag->evNum;
        ++fragmentsNumber;
    } else {
        mpdDeviceHdr.deviceId = 0xBA; // adc64 DeviceId
        mpdDeviceHdr.deviceSerial = 0;
        mpdEventHdr.evNum = 0;
    }
    mpdDeviceHdr.length = adcTrigInfo.totalDataSize + fragmentsNumber*sizeof(MpdMStreamHdr);

    mpdEventHdr.length = mpdDeviceHdr.length + sizeof(MpdDeviceHdr);

    sendHeaders();

    if(adcTrigInfo.workedChannels != adcTrigInfo.currentChannels){
        qWarning() << logPrefix() << "Incomplete Event workedChannels:"<<hex<<adcTrigInfo.workedChannels<<" currentChannels:"<<adcTrigInfo.currentChannels;
        debugPrint=true;
        ++stat.incompleteEv;
    }
    // Send trig MStream block
    quint16 minFr = 0, maxDiff, diff;
    BaseFragment *frag;
    if(adcTrigInfo.gotInfo){
        frag = adcTrigInfo.trigFrag;
        minFr = frag->fragmentId;
        maxDiff = 0;
        mpdMStreamHdr.subtype = frag->subType;
        mpdMStreamHdr.words32b = frag->curLen - (sizeof(MStreamHeader)-sizeof(MStreamFragmentHeader));
        mpdMStreamHdr.words32b /= 4;
        mpdMStreamHdr.usrDefBits = 0;
        sendData((const char *)&mpdMStreamHdr, sizeof(mpdMStreamHdr));
        sendData((const char *)(frag->data.data()+sizeof(MStreamHeader)),
                 mpdMStreamHdr.words32b*4);
        frag->clear();
    }

    // Send channals MStream blocks
    for (it=adcTrigInfo.fragments.constBegin(); it!=adcTrigInfo.fragments.constEnd(); ++it) {
        frag = it.value();
        diff = frag->fragmentId-minFr;
        if(!bigFragId) diff &= 0xFF;
        if(diff>maxDiff)
            maxDiff = diff;
        mpdMStreamHdr.subtype = frag->subType;
        mpdMStreamHdr.words32b = frag->curLen - (sizeof(MStreamHeader)-sizeof(MStreamFragmentHeader));
        mpdMStreamHdr.words32b /= 4;
        mpdMStreamHdr.usrDefBits = it.key(); // Channal Number
        sendData((const char *)&mpdMStreamHdr, sizeof(mpdMStreamHdr));
        sendData((const char *)(frag->data.data()+sizeof(MStreamHeader)),
                  mpdMStreamHdr.words32b*4);

        frag->clear();
    }
//#ifdef QT_DEBUG
//    if(lastSize!=mpdEventHdr.length){
//        qDebug()<<"Mpd Event size="<<mpdEventHdr.length<<"; previous="<<lastSize;
//        lastSize=mpdEventHdr.length;
//    }
//    qDebug()<<adcTrigInfo.evNum;
//#endif

    maxDiff =  minFr + maxDiff; // maxFr
    lastEventFragments.remove(0);
    lastEventFragments.push_back(QPair<quint16, quint16>(minFr, maxDiff) );
//    qInfo()<<"lastEventFragments.last:"<<minFr<<"-"<<maxDiff<<" evNum="<<adcTrigInfo.evNum;
    adcTrigInfo.clear();
}


void MStreamDump::closeHrb(BaseFragment &fragment, const int exactTrigPos, bool keepEvNum)
{
    // fragment.curLen - length in bytes =2*32bit+Subtype 0 Payload
    mpdMStreamHdr.subtype=fragment.subType;
    mpdMStreamHdr.words32b=fragment.curLen-(sizeof(MStreamHeader)-sizeof(MStreamFragmentHeader));
    mpdMStreamHdr.words32b /= 4;
    mpdMStreamHdr.usrDefBits=exactTrigPos;

    mpdDeviceHdr.deviceId = fragment.deviceId;
    mpdDeviceHdr.deviceSerial = fragment.deviceSerial;
    mpdDeviceHdr.length = mpdMStreamHdr.words32b*4 + sizeof(MpdMStreamHdr);

    mpdEventHdr.length = mpdDeviceHdr.length + sizeof(MpdDeviceHdr);
    if(prevEvNum >= fragment.evNum)
        lastSpillEvNum += prevEvNum+1;
    prevEvNum = fragment.evNum;
    if(keepEvNum)
        mpdEventHdr.evNum = fragment.evNum;
    else
        mpdEventHdr.evNum = fragment.evNum + lastSpillEvNum;

    sendHeaders();
    sendData((const char *)&mpdMStreamHdr, sizeof(mpdMStreamHdr));

    sendData((const char *)(fragment.data.data()+sizeof(MStreamHeader)),
              mpdMStreamHdr.words32b*4);
    fragment.clear();
}


void MStreamDump::sendHeaders()
{
    sendData((const char *)&mpdEventHdr, sizeof(mpdEventHdr));
    sendData((const char *)&mpdDeviceHdr, sizeof(mpdDeviceHdr));
    ++stat.evCount;
    stat.lastEvNum = mpdEventHdr.evNum;
}


void MStreamDump::sendData(const char *data, size_t size)
{
    if (fw){
        fw->write(data, size);
    }
    if (tcpServer){
        tcpServer->addEvent(data, size);
    }
}

void MStreamDump::addHistory(int fragId)
{
//    if(histPos==0)
//        printHistory();
    histFrags[histPos++] = fragId;
    if(histPos>=BUF_HISTORY_SIZE)
        histPos=0;
}


void MStreamDump::printHistory()
{
    QString histStr("fragment history:");
    quint16 minFrag = histFrags[histPos];
    quint16 curFrag = minFrag;
    quint16 fragRepeat=1;
    for(int pos=histPos+1;; ++pos){
        if(pos==BUF_HISTORY_SIZE)
            pos=0;
        if(pos==histPos)
            break;

        quint16 const &nextFrag = histFrags[pos];

        if(curFrag == nextFrag) {
            ++fragRepeat;
        } else {
            if(fragRepeat != 1) {
                histStr.append(QString("%1[%2] ").arg(curFrag).arg(fragRepeat));
                fragRepeat=1;
                minFrag = nextFrag;
            } else {
                if((curFrag+1) != nextFrag) {
                    if(minFrag == curFrag) {
                        if(fragRepeat==1) {
                            histStr.append(QString("%1 ").arg(curFrag));
                        }
                    } else
                        histStr.append(QString("%1->%2 ").arg(minFrag).arg(curFrag));
                    minFrag = nextFrag;
                }
            }
            curFrag = nextFrag;
        }
    }
    if(minFrag == curFrag)
        if(fragRepeat==1) {
            histStr.append(QString("%1 ").arg(curFrag));
        } else {
            histStr.append(QString("%1[%2] ").arg(curFrag).arg(fragRepeat));
        }
    else
        histStr.append(QString("%1->%2 ").arg(minFrag).arg(curFrag));

    print(histStr, true);
    print(QString("last Event fragments: %1 - %2")
          .arg(lastEventFragments.last().first)
          .arg(lastEventFragments.last().second));
}

void MStreamDump::printFragParts(quint16 fragId)
{
    print(QString("printFragParts for frId=%1:").arg(fragId));
    const BaseFragment &fr = fragments[fragId];
    if(fr.parts.isEmpty())
        print("empty");
    int checkAddr=0;
    foreach (int start, fr.parts.keys()) {
        if(checkAddr!=start)
            print(QString("Missing:%1-%2").arg(checkAddr).arg(start));
        checkAddr = fr.parts[start];
        print(QString("%1-%2").arg(start).arg(fr.parts[start]));
    }
}

void MStreamDump::sendMetrics()
{
    const QString metricPrefix = dumpAsApp ? "" : deviceIndex.getSerialStr()+".mstream.";

    //    MetricSender::send(metricPrefix+"packets.total", stat.packageCount);
    MetricSender::send(metricPrefix+"packets.valid", stat.goodFormatPackage);
    MetricSender::send(metricPrefix+"packets.error", stat.badFormatPackage);
    MetricSender::send(metricPrefix+"fragments.count", stat.fragmentCount);
    MetricSender::send(metricPrefix+"fragments.assemble.complete", stat.fragClosed);
    MetricSender::send(metricPrefix+"fragments.assemble.dropped", stat.fragDroped);
    MetricSender::send(metricPrefix+"fragments.assemble.missing", stat.fragMissed);
    MetricSender::send(metricPrefix+"fragments.assemble.incompleted", stat.fragIncompled);
    MetricSender::send(metricPrefix+"fragments.retransmit", stat.retransmitedFragParts);
    MetricSender::send(metricPrefix+"data.events", stat.evCount);
    MetricSender::send(metricPrefix+"data.bytes", stat.recvDataSize);
//    prevStat = stat;
}

void MStreamDump::waitForData()
{
    if(!mr || toDel || !waitForDataEn)
        return;
    QElapsedTimer t;
    t.start();
    int process_ev_time = 0;
    bool hasData;
    int i;
    int checkTimeCnt=0;
    const int DATA_SEM_TIMEOUT = 10;
    while(true) {
        hasData = mrDataSizeSem->tryAcquire(1, DATA_SEM_TIMEOUT);
        if(hasData) {
            int n = mrDataSizeSem->available();
            if(n)
                mrDataSizeSem->acquire(n);
            int task_size = n+1; // add one from tryAcquire
            for(i=0; i<task_size; ++i) {
                const Task &task = mr->getPendingTask(i);
                if(task.dataValid)
                    processData(task.buf, task.len, task.fi);
            }
            mr->releaseDataSem(task_size);
            checkTimeCnt += task_size;
        } else {
            checkTimeCnt += DATA_SEM_TIMEOUT;
        }
        if(checkTimeCnt>100) {
            int time = t.elapsed();
            if(time > 1000)
                break;
            if(time-process_ev_time > 10) {
                QCoreApplication::processEvents(QEventLoop::AllEvents, 1);
                if(toDel || !waitForDataEn)
                    return;
                process_ev_time = time;
            }
            checkTimeCnt=0;
        }
    }

    waitDataTimer->start();
}

void MStreamDump::delete_dump(const DeviceIndex &index)
{
    if(!index.isNull() && index.getSerial() != deviceIndex.getSerial())
        return;
    toDel=true;
    deleteLater();
}

void MStreamDump::msdump_reset(const DeviceIndex &index, bool _bigFragId,
                              quint16 _hwBufSize, quint16 _ackSizeLimit, QString fn, bool doStatClear)
{
    if(index != deviceIndex)
        return;
    bigFragId = _bigFragId;
    hwBufSize = _hwBufSize;
    ackSizeLimit = _ackSizeLimit;
    preRunInit();

    const int frSize = bigFragId ? FRAGMENTS_SIZE_16_BIT : FRAGMENTS_SIZE_8_BIT;
    fragments.resize(frSize);
    for (int i=0;i<frSize; ++i) {
        fragments[i].clear();
        fragments[i].fragmentId = i;
    }

    adcTrigInfo.clear();

    lastEventFragments.clear();
    lastEventFragments.resize(20);

    histFrags.clear();
    histFrags.resize(BUF_HISTORY_SIZE);
    histPos=0;

    fragmentHdr= nullptr;
    lastClosedFragmentId = -1;
    initStage = true;

    lastSpillEvNum = 0;
    prevEvNum = 0;

    if(doStatClear)
        stat.clear();

    if(mr)
        createReciever();
    emit reset();

    if(fn != fileName) {
        delete fw;
        fileName = fn;
        fw = fileName.isNull() ? nullptr : new MStreamFileWriter(fileName.toLocal8Bit());
    }
}

void MStreamDump::send_connect_to_dev(const DeviceIndex &index)
{
    if(index != deviceIndex)
        return;
    emit connectToHardware();
}

void MStreamDump::printStat()
{
#ifdef Q_OS_UNIX
    if(dumpAsApp) {
        const pid_t pId = getppid();
        if (pId==1) {
            qWarning() << logPrefix() + "No parent process, exiting";
            exit(1);
        }
    }
#endif
//    if(statTimer->elapsed() > RUN_LOOP_PERIOD_MS){
        if(dumpAsApp){
            qint64 timeMs = qMax(statTime->restart(), 1);
            QString mess = QString("[stat]packages=%1;packLastSec=%8;retransmitCount=%2;"
                                   "evCount=%10;evCountLastSec=%11;lastEv=%12;"
                                   "fragAll=%5;fragDroped=%6;fragMissed=%7;"
                                   "recvSize_B=%3;recvSizeLastSec_kBs=%4;"
                                   "upTimeSec=%9")
                    .arg(stat.packageCount).arg(stat.retransmitedFragParts)
                    .arg(stat.recvDataSize).arg(1.*(stat.recvDataSize-prevStat.recvDataSize)/timeMs)
                    .arg(stat.fragTotal).arg(stat.fragDroped).arg(stat.fragMissed)
                    .arg((stat.packageCount-prevStat.packageCount)*1000/timeMs)
                    .arg(stat.upTime.elapsed()/1000)
                    .arg(stat.evCount).arg((stat.evCount-prevStat.evCount)*1000/timeMs)
                    .arg(stat.lastEvNum);

            if(stat.badFormatPackage)
                mess.append(QString(";badFormatPackages=%1").arg(stat.badFormatPackage));
            if(stat.missedEventsRanges) {
                mess.append(QString(";missedEvents=%1;missedEventsRanges=%2")
                            .arg(stat.missedEvents)
                            .arg(stat.missedEventsRanges));
            }
            if(stat.fragIncompled) {
                mess.append(QString(";fragIncompled=%1")
                            .arg(stat.fragIncompled));
            }
            if(tcpServer)
                mess.append(QString(";tcpOccupancy=%1").arg(tcpServer->getOccupancy(),0,'f',2));

            print(mess, true);
            std::cerr << std::endl;
        }
        stat.timestamp.start();
        emit stat_updated(stat);
        prevStat = stat;
        //    }
//        stat.time.start();
}

void MStreamDump::initialize()
{
    qInfo().noquote() << logPrefix()  << "initialization";
    initStage=false;

    quint16 minFragId = fragmentHdr->getFragId(bigFragId);
    quint16 frId=minFragId-1;
    quint16 fragLimit = minFragId-getAcceptedRange()-1;
    quint16 fragLimit2 = minFragId+getAcceptedRange()-1;
    if(!bigFragId) {
        frId &= 0xFF;
        fragLimit &= 0xFF;
    }
    for( ; frId!=fragLimit; ) {
        Q_ASSERT(frId != fragmentHdr->getFragId(bigFragId));
        if(!fragments[frId].free){
            minFragId = frId;
        }
        --frId;
        if(!bigFragId) frId &= 0xFF;
    }
    lastClosedFragmentId = minFragId-1;

    for( ; frId!=fragLimit2; ) {
        if(!fragments[frId].free){
            fragments[frId].clear();
        }
        --frId;
        if(!bigFragId) frId &= 0xFF;
    }

    if(!bigFragId) lastClosedFragmentId &= 0xFF;
//    // Find out first eadge of fragment that would be lastClosedFragmentId
//    for(quint8 frId=fragmentHdr->fragmentId-1; frId!=fragmentHdr->fragmentId; --frId){
//        if(fragments[frId].free){
//            lastClosedFragmentId = frId;
//            break;
//        }
//    }

//    // All previous fragments will be removed
//    for(quint8 frId=lastClosedFragmentId-1; frId!=fragmentHdr->fragmentId; --frId){
//        if(!fragments[frId].free){
//            qDebug()<<"remove previous fragments fId="<<frId;
//            fragments[frId].clear();
//        }
//    }

    // Defines gap in intialization loop
//    QVectorIterator<BaseFragment> i(fragments);
//    QList<quint8> ids;
//    while(i.hasNext()){
//        const BaseFragment &f = i.next();
//        if (f.free)
//            continue;
//        ids.append(f.fragmentId);
//    }
//    quint8 min = ids[0];
//    quint8 max = ids[ids.size()-1];
//    quint8 halfLoop= -1;
//    halfLoop /=2;
//    if (max-min > halfLoop){
//        min = -1;
//        max = 0;
//        foreach (quint8 id, ids) {
//            if (id < halfLoop){
//                // candidate for max
//                if (max < id)
//                    max = id;
//            } else {
//                // candidate for min
//                if (min > id)
//                    min = id;
//            }
//        }
//    }
//    bool hasGap=false;
//    int id=min;
//    for (int i=0;i<HW_BUF_SIZE;++i){
//        if (!ids.contains(id)){
//            hasGap=true;
//            break;
//        }
//        ++id;
//    }
//    if (hasGap){
//        qDebug() << "There is gap "<<id<<" in fragmentIds:"<<ids;
//    } else {
//        qDebug() << "All first fragments are in range:"<<min<<"-"<<max;
//    }
//    lastClosedFragmentId = min-1;

    qInfo().noquote() << logPrefix() +  QString(" set lastClosedFragmentId to %1 curFragId=%2").arg(lastClosedFragmentId).arg(fragmentHdr->getFragId(bigFragId));

    // Try to Close gained fragments
    quint16 nextFrId = lastClosedFragmentId+1;
    if(!bigFragId) nextFrId &= 0xFF;
    if (fragments[nextFrId].completed)
        closeFragment(nextFrId);
}

void MStreamDump::checkDeviceSerial(uint32_t serial)
{
    const auto &deviceSerial = deviceIndex.getSerial();
    if(serial==deviceSerial)
        return;

    if(serial==0) {
        qInfo().noquote() << logPrefix() + "Missing device serial number";
    } else if (deviceSerial) {
        qInfo().noquote() << logPrefix() << QString("Device serial number changed from 0x%1 to 0x%2")
            .arg(getSerialIdStr(true)).arg(serial, 8, 16,QChar('0'));
    } else {
        qInfo().noquote() << logPrefix() << QString("Device serial number set to 0x%1")
            .arg(getSerialIdStr(true));
    }
    deviceIndex = DeviceIndex(deviceIndex.getDevId(), serial);
}

void MStreamDump::debugPrintBufInHex(const char *buf, int size)
{
#ifndef QT_DEBUG
    return;
#endif
    const quint32 *d;
    int pos=0;
    QStringList mess;
    while(pos<size){
        d = reinterpret_cast<const quint32*>(buf+pos);
        mess.append(QString("%1: %2").arg(pos, 4, 10, QChar('0')).arg(*d, 4, 16, QChar('0')));
        pos += sizeof(quint32);
    }
    qDebug() << mess.join(" ");
}

void MStreamDump::updateProgDescription()
{
    progDescr.type = DaqConfig::getTypeName(DaqConfigMStream);
    progDescr.index = getSerialIdStr(true).prepend("0x").toLower();
    progDescr.name = progDescr.type +" "+ progDescr.index;
#ifdef GIT_DESCR
    progDescr.ver_hash = GIT_DESCR;
#endif
#ifdef GIT_LAST_COMMIT
    progDescr.ver_date = GIT_LAST_COMMIT;
#endif
    progDescr.options.clear();
    progDescr.options["dev_serial"] = progDescr.index;
    progDescr.options["dev_id"] = QString("0x%1")
            .arg(deviceIndex.getDevId(), 2, 16, QChar('0'));
    if(!deviceAddress.isNull()) {
        progDescr.options["dev_address"] = deviceAddress.toString();
        progDescr.options["out_port"] = QString::number(tcpPort);
    }
    if(mr)
        progDescr.options["myReceiverPort"] = QString::number(myReceiverPort);
    if(!fileName.isEmpty())
        progDescr.options["fileName"] = fileName;
    progDescr.interfaces.clear();
    if(tcpStatus.tcpPort) {
        ProgramInterface progIf;
        progIf.setType(ProgramInterfaceDataFlow);
        progIf.port = tcpStatus.tcpPort;
        progIf.isFree = tcpStatus.isFree;
        progIf.enabled = true;
        if(!tcpStatus.isFree)
            progIf.addPeer(tcpStatus.conHost, tcpStatus.conPort);
        progDescr.interfaces.push_back(progIf);
    }

    emit updateProgramDescription(progDescr);
}

void MStreamDump::createReciever()
{
    if(mr)
        destroyReciever();
    if(deviceAddress.isNull()) {
        statTimer->start(100);
        return;
    }

    mr = new MlinkStreamReceiver(deviceIndex, deviceAddress.toString().toLatin1(),
                                 bigFragId, hwBufSize, dumpAsApp);
    mrDataSizeSem = mr->getDataSizeSem();
    myReceiverPort = mr->getLocalPort();
    mr->setAckSizeLimit(ackSizeLimit);
    connect(this, &MStreamDump::connectToHardware, mr, &MlinkStreamReceiver::connectToHardware);
    connect(this, &MStreamDump::closeReciever, mr, &MlinkStreamReceiver::deleteReceiver);
    connect(mr, &MlinkStreamReceiver::destroyed, receiverThread, &QThread::quit);
#if (! SEM_INTERACT)
    connect(this, &MStreamDump::releaseData, mr, &MlinkStreamReceiver::releaseData);
    connect(mr, &MlinkStreamReceiver::gotData, this, &MStreamDump::gotData);
#endif
    connect(mr, &MlinkStreamReceiver::stat_from_receiver_updated, this, &MStreamDump::stat_from_receiver_updated);

    mr->moveToThread(receiverThread);
    receiverThread->start();
    receiverThread->setPriority(QThread::HighPriority);
#if SEM_INTERACT
    waitDataTimer->start();
#endif

    emit connectToHardware();
    waitForDataEn=true;
}

void MStreamDump::destroyReciever()
{
    waitForDataEn=false;
    if(!mr)
        return;

    mr = nullptr;
    mrDataSizeSem = nullptr;
    emit closeReciever();
    if(!threadQuiting(receiverThread, 30000)) {
        qWarning().noquote() << logPrefix() << "MLink receiver thread failed to quit quietly";
    } else {
        qDebug().noquote() << logPrefix() << "MStreamDump::destroyReciever - success";
    }
    waitDataTimer->stop();
}

void MStreamDump::print(QString s, bool noLog)
{
#ifdef _WIN32
    std::cerr<<s.toStdString();
#else
#if QT_VERSION < QT_VERSION_CHECK(5, 5, 0)
    qDebug()<<s;
#else
    if(noLog)
        std::cerr<<s.toStdString();
    else
        qInfo()<<s;
#endif // QT_VERSION
#endif // _WIN32
}

/*
 * = HRB =
 * == Recive ==
 * [MStreamHeader]
 *  [MStreamFragmentHeader]
 *   fragId+fragOffset
 *   devId+flags+subType+fragLen
 *  [/MStreamFragmentHeader]
 *  evNum+userDefBits
 *  devSerial
 * [/MStreamHeader]
 *
 * [RawData]
 *  [TS]
 *   tsSec
 *   tsNSec
 *  [/TS]
 *  [Samples/]
 * [/RawData]
 *
 * /// size(RawData)=fragLen-2
 *
 * == Send ==
 * [MpdEventHdr/]
 * [MpdDeviceHdr/]
 * [MpdMStreamHdr/]
 * [frag]
 *  trigPos+Len+SubType
 *  [RawData]
 *   [TS]
 *    tsSec
 *    tsNSec
 *   [/TS]
 *   [Samples/]
 *  [/RawData]
 * [/frag]
 *
 *
 *
 * = ADC64 =
 * == Recive ==
 * [MStreamHeader]
 *  [MStreamFragmentHeader]
 *   fragId+fragOffset
 *   devId+flags+subType+fragLen
 *  [/MStreamFragmentHeader]
 *  evNum+userDefBits
 *  devSerial
 * [/MStreamHeader]
 *
 * [RawData/]
 *
 *
 * fragLen=2+size(RawData)
 *
 * == Send ==
 * [MpdEventHdr/]
 * [MpdDeviceHdr/]
 * [MpdMStreamHdr/]
 * [frag]
 *  trigPos+Len+SubType
 *  [RawData/]
 * [/frag]
 *
 */
