//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef MSTREAMFILEWRITER_H
#define MSTREAMFILEWRITER_H

#include <cstdio>

class MStreamFileWriter {
public:
    MStreamFileWriter(const char *filename);
    void write(const char *buf, size_t len);
    ~MStreamFileWriter();
protected:
    FILE *f;
};

#endif // MSTREAMFILEWRITER_H
