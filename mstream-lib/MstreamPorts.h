//
//    Copyright 2015 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef MSTREAMPORTS_H
#define MSTREAMPORTS_H

#include <QtGlobal>

const quint16 MSTREAM_PRIMARY_BASE_TCP_PORT_VMEDAQ   = 33400; // primary stream would block readout process
const quint16 MSTREAM_SECONDARY_BASE_TCP_PORT_VMEDAQ = 33410; // secondary stream events may be dropped by server

#endif // MSTREAMPORTS_H
