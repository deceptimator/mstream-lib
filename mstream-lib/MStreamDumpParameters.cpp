#include "MStreamDumpParameters.h"

#include <QCommandLineParser>
#include <QDebug>
#include <QHostInfo>

#include <iostream>
#include "metric-sender/MetricSender.h"

MStreamDumpParameters::MStreamDumpParameters() :
    tcpPort(0),
    bigFragId(false),
    hwBufSize(8),
    ackSizeLimit(1),
    acceptedRange(0)
{
}

QString MStreamDumpParameters::logPrefix() const
{
    return QString("[MStream] [%1]: ").arg(getSerialIdStr());
}

QString MStreamDumpParameters::getSerialIdStr(bool shortFormat) const
{
    if(shortFormat)
        return QString("%1").arg(deviceIndex.getSerial(), 8, 16, QChar('0')).toUpper();
    else
        return deviceIndex.getSerialStr();
}

void MStreamDumpParameters::parseCommandLine()
{
#ifdef GIT_DESCR
    {
        QString version = QString(GIT_DESCR);
        if(!version.isEmpty()) {
            version.prepend("MStream version : ");
            std::cerr << version.toStdString() << std::endl;
        }
    }
#endif

    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::applicationName());
    const QCommandLineOption helpOpt = parser.addHelpOption();
    parser.addPositionalArgument("device", "hexadecimal serial number of source device");

    QCommandLineOption mStreamVerOpt(QStringList() << "v" << "version",
                                       "MStream verion of incoming data (default: 2.1)",
                                       "versionStr");
    parser.addOption(mStreamVerOpt);

    QCommandLineOption hwBufSizeOpt(QStringList() << "hwBufSize",
                                       QString("hwip buffer size in device (default %1)").arg(hwBufSize),
                                       "hwBufSize");
    parser.addOption(hwBufSizeOpt);

    QCommandLineOption ackSizeOpt(QStringList() << "ackSize",
                                  QString("Maximum number of acknowledge fragments per packet (default: %1)")
                                  .arg(ackSizeLimit),
                                  "ackSizeLimit");
    parser.addOption(ackSizeOpt);

    QCommandLineOption fileOpt(QStringList() << "f" << "file",
                                       "path to file writer",
                                       "filePath");
    parser.addOption(fileOpt);

    QCommandLineOption tcpPortOpt(QStringList() << "p" << "port",
                                  QString("TCP-port number for outgoing data (default: %1)").arg(tcpPort),
                                  "tcpPort");
    parser.addOption(tcpPortOpt);

    parser.process(QCoreApplication::arguments());

    if(parser.isSet(helpOpt))
        parser.showHelp();

    bool argOk = true;
    bool valOk;
    QString valStr;

    const QStringList positionalArgs = parser.positionalArguments();
    if(positionalArgs.size() != 1) {
        qWarning() << "undefined positional arguments:" << positionalArgs;
        argOk = false;
    }
    if(argOk) {
        valStr = positionalArgs.first().trimmed();
        quint64 serial_id = 0;
        serial_id = valStr.toULongLong(&valOk, 16);
        if(!valOk) {
            deviceAddress = QHostAddress(valStr);
            if(deviceAddress.isNull()) {
                QHostInfo info = QHostInfo::fromName(valStr);
                QList<QHostAddress> addrList = info.addresses();
                if (addrList.isEmpty()) {
                    qCritical() << QString("Can't find IP address of host '%1': %2")
                                   .arg(valStr)
                                   .arg(info.errorString());
                    argOk = false;
                } else
                    deviceAddress = info.addresses().at(0);
            }
        }
        deviceIndex = DeviceIndex(deviceIndex.getDevId(), serial_id);
    }

    if(argOk && parser.isSet(mStreamVerOpt)) {
        valStr = parser.value(mStreamVerOpt);
        uint maj=0,min=0;

        QStringList parts = valStr.split('.');
        argOk = parts.size() == 2;

        if(argOk)
            maj = parts[0].toUInt(&argOk);
        if(argOk)
            min = parts[1].toUInt(&argOk);
        if(argOk) {
            if(maj>2 || (maj==2 && min>=2)) {
                bigFragId = true;
            }
            qInfo() << "Set MStream version with"
                    << (bigFragId ? "big (16-bit)" : "small (8-bit)")
                    << "fragmentId";
        } else
            qWarning() << "undefined version:" << valStr;
    } else
        std::cerr << "using mstream 2.0 version";

    if(argOk && parser.isSet(tcpPortOpt)) {
        valStr = parser.value(tcpPortOpt);
        tcpPort = valStr.toInt(&valOk);
        if (!valOk || tcpPort <= 0) {
            qWarning() << QString("bad option '%1' value: %2")
                          .arg(tcpPortOpt.names().first()).arg(valStr);
            argOk = false;
        } else {
            qInfo() << "Set tcpPort to " << tcpPort;
        }
    }

    if(argOk && parser.isSet(hwBufSizeOpt)) {
        valStr = parser.value(hwBufSizeOpt);
        hwBufSize = valStr.toUShort(&valOk);
        if (!valOk) {
            qWarning() << QString("bad option '%1' value: %2")
                          .arg(hwBufSizeOpt.names().first()).arg(valStr);
            argOk = false;
        } else {
            qInfo() << "Set hwBufSize to " << hwBufSize;
        }
    } else
        std::cerr << QString("Hw buf size:%1").arg(hwBufSize).toStdString();

    if(argOk && parser.isSet(ackSizeOpt)) {
        valStr = parser.value(ackSizeOpt);
        ackSizeLimit = valStr.toUShort(&valOk);
        if (!valOk) {
            qWarning() << QString("bad option '%1' value: %2")
                          .arg(ackSizeOpt.names().first()).arg(valStr);
            argOk = false;
        } else {
            if(ackSizeLimit == 0)
                ackSizeLimit = hwBufSize;
            qInfo() << "Set ackSizeLimit to " << ackSizeLimit;
        }
    }

    if(argOk && parser.isSet(fileOpt)) {
        fileName = parser.value(fileOpt);
        qInfo() << "Set fileName to " << fileName;
    }

    if(!argOk) {
        parser.showHelp();
    }

    preRunInit(true);
}

void MStreamDumpParameters::preRunInit(bool doMetricSenderInit)
{
    if(bigFragId)
        acceptedRange = 2*(hwBufSize-1) * FRAGMENTS_IN_PACKAGE_2_2; // was 2*HW_BUF_SIZE
    else
        acceptedRange = hwBufSize * FRAGMENTS_IN_PACKAGE_2_0; // was 2*HW_BUF_SIZE

    if(doMetricSenderInit) {
        // Initialize MetricSender in case of afi-mstream app
        const QString shortHostName = QHostInfo::localHostName().section(".", 0, 0);
        MetricSender::setNamespace("daq.mstream." + shortHostName + "." + getSerialIdStr(true));
    }
}
