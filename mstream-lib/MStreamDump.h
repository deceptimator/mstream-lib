//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef MSTREAMDUMP_H
#define MSTREAMDUMP_H

#include <QObject>
#include <QVector>

#include "MStreamDumpParameters.h"
#include "pnp-server/PNPMessage.h"
#include "tcpserver/TcpServer.h"
#include "MlinkStreamReceiver.h"
#include "lib-common/MpdRawTypes.h"

class QThread;
class QTimer;

class LldpInfoCache;
class DeviceIndex;
class MStreamFileWriter;
class MldpListener;
class MlinkStreamReceiver;
class PNPServer;
class RawCheck;
class QSemaphore;

struct MStreamStat{
    MStreamStat() { clear(); }

    void clear() {
        retransmitedFragParts=0;
        recvDataSize=0;
        packageCount=0;
        fragmentCount=0;
        fragTotal=0;
        fragClosed=0;
        fragDroped=0;
        fragMissed=0;
        fragIncompled=0;
        goodFormatPackage=0;
        badFormatPackage=0;
        missedEvents=0;
        missedEventsRanges=0;
        incompleteEv=0;
        evCount=0;
        lastEvNum=0;
        upTime.start();
    }

    DeviceIndex index;
    quint64 packageCount;
    quint64 recvDataSize;
    quint64 fragmentCount;
    quint64 retransmitedFragParts; // packages with lastClosed-Range<frId<lastClosed || dublicate start/end
    quint64 fragTotal;
    quint64 fragClosed;
    quint64 fragDroped; // has incompleted size or it is a chFrame without trigFrame
    quint64 fragMissed; // has incompleted size or it is a chFrame without trigFrame
    quint32 fragIncompled; // incompleted fragments //#
    quint64 goodFormatPackage;
    quint64 badFormatPackage;
    quint64 missedEvents;
    quint64 missedEventsRanges;
    quint32 incompleteEv;
    quint64 evCount;
    quint64 lastEvNum;
    QElapsedTimer upTime;
    QElapsedTimer timestamp;
    TcpServerStatus tcpStatus;
};
Q_DECLARE_METATYPE(MStreamStat)

class MStreamDump : public QObject , protected MStreamDumpParameters
{
    Q_OBJECT
public:
    explicit MStreamDump(MStreamDumpParameters msParams, bool _doPrintStat = true);
    virtual ~MStreamDump();
    void terminate();

public slots:
    void msdump_reset(const DeviceIndex &index, bool _bigFragId, quint16 _hwBufSize, quint16 _ackSizeLimit, QString fn, bool doStatClear);
    void lldpCacheUpdated(const LldpInfoCache &dd);
    void delete_dump(const DeviceIndex &index);
    void send_connect_to_dev(const DeviceIndex &index);

private slots:
    void start();
    void printStat();
    void gotData(const QVector<Task> &tasks);
    void tcpServerStatusUpdated(const TcpServerStatus &status);
    void sendMetrics();
    void waitForData();

signals:
    void updateProgramDescription(const ProgramDescription&) const;
    void closing(const ProgramDescription&) const;
    void closeReciever();
    void connectToHardware();
    void releaseData(int);
    void stat_updated(MStreamStat stat);
    void stat_from_receiver_updated(ReceiverStat stat);
    void reset();

private:
    void processData(const char *globalBuf, int recvSize, const MLinkFrameInfo &fi);
    void closeFragment(quint16 fragmentId, bool force=false);
    void closeAdc();
    void closeHrb(BaseFragment &fragment, const int exactTrigPos, bool keepEvNum=true);
    void sendHeaders();
    void sendData(const char *data, size_t size);
    void addHistory(int fragId);
    void printHistory();
    void printFragParts(quint16 fragId);
    void initialize();
    void checkDeviceSerial(uint32_t serial);
    void debugPrintBufInHex(const char *buf, int size);
    void updateProgDescription();
    void createReciever();
    void destroyReciever();
    void print(QString s, bool noLog=false);

    ProgramDescription progDescr;
    MlinkStreamReceiver * mr;
    QSemaphore *mrDataSizeSem;
    QThread * const receiverThread;
    MStreamFileWriter * fw;
    QThread * const tcpServerThread;
    TcpServer * const tcpServer;
    bool debugPrint;


    // < <deviceId, subtype>, <eventNum:24, <fragmentId, fragmentData> > >
//    QMap<DeviceKey,  BaseDevice> devices;
    QVector<BaseFragment> fragments; // <fragmentId (quint32:24), BaseEvent>
    AdcEventTrigInfo adcTrigInfo;

    MpdEventHdr mpdEventHdr;
    MpdDeviceHdr mpdDeviceHdr;
    MpdMStreamHdr mpdMStreamHdr;

    QByteArray rawData;
    const MStreamFragmentHeader *fragmentHdr;
//    quint16 deviceId;
    quint16 lastClosedFragmentId;
    bool initStage;
    QVector<quint16> histFrags;
    int histPos;
    MStreamStat prevStat, stat;
    QTime * const statTime;
    QTimer * const statTimer;
    QTimer * const waitDataTimer;
    QTimer * const metricTimer;
    QSet<int> initPackageSet;
    QVector<QPair<quint16,quint16> > lastEventFragments;
    quint32 lastSpillEvNum; // for HRB only
    quint32 prevEvNum; // for HRB only
    bool isDevBusy;
    int dataIndex;
    TcpServerStatus tcpStatus;
    uint16_t myReceiverPort;
    bool toDel;
    bool waitForDataEn=false;
    bool dumpAsApp;
};

#endif // MSTREAMDUMP_H
