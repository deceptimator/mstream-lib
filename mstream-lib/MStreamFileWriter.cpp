//
//    Copyright 2012 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "MStreamFileWriter.h"

#include <stdexcept>

MStreamFileWriter::MStreamFileWriter(const char *filename)
{
    f = fopen(filename, "a+b");
    if (!f) {
        perror("fopen");
        throw std::runtime_error("fopen error");
    }
    fprintf(stderr, "MStreamFileWriter opened file: %s\n", filename);
}

MStreamFileWriter::~MStreamFileWriter()
{
    fclose(f);
}

void MStreamFileWriter::write(const char *buf, size_t count)
{
    try {

        size_t result = fwrite(buf, count, 1, f);
        if (result == 0) {
            perror("fwrite");
        }
    } catch (const std::exception &e) {
        perror(e.what());
    } catch(...) {
        perror("fwrite");
    }
}
